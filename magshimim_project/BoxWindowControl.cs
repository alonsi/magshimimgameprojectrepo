﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

//This will be attached to the box window object.
//will populate the window with the image of the treasure
//will populate the window with the steal button.
public class BoxWindowControl : MonoBehaviour {

	Box _box; //the box this window represents

	//returns a BoxWindowControl instance, linked to the parameter box.
	public static BoxWindowControl Create(Box box)
	{
		//instantiate the prefab
		GameObject boxWindowGo = Instantiate(box.BOX_WINDOW_PREFAB, box._playerUi);

		//extract the script
		BoxWindowControl boxWindow = boxWindowGo.GetComponent<BoxWindowControl>();

		//initialize the script
		boxWindow._box = box;

		return boxWindow;
	}

	// Use this for initialization
	void Start ()
	{
		//Populates the window with the apropriate image to the valuable in the box.
		GameObject ValuablePrefab = GetValuableGameObject(this._box.Valuable);
		Instantiate(ValuablePrefab, this.transform);
	}

	//TODO add the rest of the gameobjects and implement them here
	private static GameObject GetValuableGameObject(ValuablesEnum valuableToReturn)
	{
		switch (valuableToReturn)
		{
			case ValuablesEnum.DIAMOND_RING:
				return MyResources.Singleton.DiamondRingPrefab;
			default:
				Debug.LogError("tried to populate box window with bad valuable input");
				return null;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
