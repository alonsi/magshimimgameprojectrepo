﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class NameInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
		InputField inputField = GetComponent<InputField>();
		Assert.IsNotNull(inputField, "couldn't find the name InputField");
		inputField.onValueChanged.AddListener(PlayerPreferences.Singleton.UpdateName);
	}
}
