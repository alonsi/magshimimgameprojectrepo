﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class PearlNecklace : Valuable
{
    public PearlNecklace()
    {
        this.ScoreValue = 25;
        this.SecondsToSteal = 2;
        this.ValuableType = ValuablesEnum.PEARL_NECKLACE;
    }
}
