﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//TODO seperate into 3 scripts:
// UI
// Round and Scores
// Networking
public class RoomManager : NetworkBehaviour
{
	public List<PendingPlayer> _players;
	public static RoomManager _singleton = null;
	int _roundCount;
	int _numberOfRounds;
	private const int NUMBER_OF_SECONDS_TO_WAIT_BEFORE_CLOSING_ROOM_AS_THE_MATCH_CLOSES = 5;
	PlayerTypeOptions _playerTypeOptions;
	Coroutine _leaderboardUpdateRoutine;
	Text _statusText;

	public int _numberOfExpectedPlayers; //once x amount of players connect, should start the game.

	//use these variables to turn on or off certain character types (but there will probably be bugs if you try to split pairs (killer on and bodyguard off for example)).
	public const bool WITH_KILLER = true;
	public const bool WITH_BODYGUARD = true;
	public const bool WITH_THIEF = true;
	public const bool WITH_SECURITY = true;

	private void Start()
	{
		SetupUI();
		//if a roomManager already exists...
		//only adds the event listener if this an original RoomManager and not an excess one.
		//see SceneLoadedCallBack function
		SceneManager.sceneLoaded += SceneLoadedCallBack; //TODO just on server?

		if (isServer)
		{
			customNetworkManager._singleton.ClientDisconnected += OnClientDisconnect;
		}
		
		_roundCount = 0;
		DontDestroyOnLoad(this);

		_playerTypeOptions = new PlayerTypeOptions();
	}

	[Server]
	private void OnClientDisconnect()
	{
		print("onclientdisconnect");
		LeaveRoom();
	}

	private void Awake()
	{
		if (_singleton != null)
		{
			//destroy it
			print("destroying excess roomanager");
			Destroy(this.gameObject);
		}
		else
		{
			_numberOfExpectedPlayers = 0;
			if (WITH_BODYGUARD) _numberOfExpectedPlayers++;
			if (WITH_KILLER) _numberOfExpectedPlayers++;
			if (WITH_THIEF) _numberOfExpectedPlayers++;
			if (WITH_SECURITY) _numberOfExpectedPlayers++;

			_numberOfRounds = _numberOfExpectedPlayers;
			_singleton = this;
			_players = new List<PendingPlayer>();
		}
	}

	public override void OnStartServer()
	{
		Assert.IsNotNull(customNetworkManager._singleton);
		customNetworkManager._singleton.ClientConnected += OnClientConnect;
		base.OnStartServer();
	}

	private void OnDestroy()
	{
		if (this == _singleton)
		{
			_singleton = null;
			//PlayerTypeOptions.Reset(); //reset the PlayerTyopeOptions so we could use it again on the next Room.
			PendingPlayer.ResetNameSuffix(); //reset to player1, player2...
			//unregister only if it's the original, used instance that is being destroyed (and not the excess clone)
			SceneManager.sceneLoaded -= SceneLoadedCallBack;
			customNetworkManager._singleton.ClientConnected -= OnClientConnect;
		}
	}

	private void OnDisable()
	{
		StopAllCoroutines();
	}

	/// <summary>
	/// checks if this is a viable request (it's the host, and there are enough players in the room).
	/// then, starts the game by switching to game scene.
	/// </summary>
	private void StartGameFromRoom()
	{
		if (!isServer) //only host can start the game
		{
			print("ignores client trying to start game (only host can start the game)");
		}
		else if (_players.Count != _numberOfExpectedPlayers) //only starts if reached the expected number of players.
		{
			print("can't start game yet cus not enough players");

			//updates statusText to show an error message
			Assert.IsFalse(_statusText.IsDestroyed(), "trying to access a destroyed _statusText reference");
			_statusText.text = "can't start game yet cus not enough players";
		}
		else
		{
			_roundCount++;
			AssignClassesToPlayers();

			//start the game by switching to game scene.+
			SceneManager.LoadScene("_GameScene"); //This fixed the bug where a client wouldn't load the scene in time, and his scene will be messed up.
			customNetworkManager._singleton.ServerChangeScene("_GameScene");
		}
	}

	/// <summary>
	/// called whenever a scene has loaded
	/// this isn't invoked on the first the the room is loaded (because this function isn't registered with the event yet)
	/// </summary>
	/// <param name="loadedScene"></param>
	/// <param name="loadMode"></param>
	public void SceneLoadedCallBack(Scene loadedScene, LoadSceneMode loadMode)
	{
		if (loadedScene.name == "RoomScene")
		{
			SetupUI();
			if (_roundCount == _numberOfRounds)
				StartCoroutine(EndMatch());
		}
		else if (loadedScene.name == "_LobbyScene")
		{
			print("sceneloadedcallback: destroying roommanager (cus moving to lobby)");
			Destroy(this.gameObject);
		}
		else if (loadedScene.name == "_GameScene")
		{

		}
		else
		{
			print("loaded an unrecognizable scene");
		}
	}

	/// <summary>
	/// chooses the winner, and starts the UpdateStatusTextLoop (showing that player's name)
	/// waits for a certain number of seconds, and then closes the room.
	/// </summary>
	/// <returns></returns>
	private IEnumerator EndMatch()
	{
		PendingPlayer highestScorePlayer = GetPlayerWithHighestScore();
		Assert.IsNotNull(highestScorePlayer, "couldn't find a highestScorePlayer");

		//makes sure the _statsText can be accessed
		Assert.IsNotNull(_statusText, "trying to access a nulled _statusText");
		Assert.IsFalse(_statusText.IsDestroyed(), "trying to access a destroyed _statusText reference");

		//starts the loop that shows the name of the winner with highest score player's name
		StartCoroutine(UpdateStatusTextLoop("winner is: " + highestScorePlayer._name));

		//waits for a certain amount of seconds before closing the room.
		yield return new WaitForSeconds(NUMBER_OF_SECONDS_TO_WAIT_BEFORE_CLOSING_ROOM_AS_THE_MATCH_CLOSES);
		LeaveRoom();
	}

	/// <summary>
	/// see return
	/// </summary>
	/// <returns>the player with the highest score from the list of players in room</returns>
	private PendingPlayer GetPlayerWithHighestScore()
	{
		PendingPlayer highestScorePlayer = null;
		int highestScore = 999;

		Assert.AreEqual(_players.Count, RoomManager._singleton._numberOfExpectedPlayers, "list of players doesn't have the expected number of players");

		foreach (PendingPlayer currPlayer in _players)
		{
			//if the next player is the first one, of if his score is bigger then the former player.
			if (currPlayer._score > highestScore || highestScorePlayer == null)
			{
				//sets him as the highestscore player
				highestScore = currPlayer._score;
				highestScorePlayer = currPlayer;
			}
		}

		Assert.IsNotNull(highestScorePlayer, "couldn't find a highestScorePlayer");
		return highestScorePlayer;
	}

	/// <summary>
	/// updates the statusText with the parameter.
	/// this is used as a loop because the Rpc didn't work otherwise.
	/// TODO maybe the reason this works instead of just a one-time rpc call is because there is this function in the middle,
	/// ... try using a non looping routine "middle" function instead.
	/// </summary>
	/// <param name="newText"></param>
	/// <returns></returns>
	private IEnumerator UpdateStatusTextLoop(string newText)
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			RpcUpdateStatusText(newText);
		}
	}

	/// <summary>
	/// updates the status text according to the parameter on all clients
	/// </summary>
	/// <param name="newText"></param>
	[ClientRpc]
	private void RpcUpdateStatusText(string newText)
	{
			//yield return new WaitForSeconds(1);
			print("in rpcupdatestatus text");
			_statusText.text = newText;
	}

	private IEnumerator UpdateLeaderBoardLoop()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			RpcUpdateLeaderBoardText(GetLeaderBoardText());
		}
	}

	private void SetupUI()
	{
		//print("in setupUI");
		if (isServer)
		{
			SetupStartGameButton();
		}

		SetupLeaveRoomButton();
		SetupStatusText();
	}

	private void SetupStatusText()
	{
		//print("in setupstatustext");
		GameObject statusTextGO = GameObject.Find("statusText");
		Assert.IsNotNull(statusTextGO, "couldn't find statusText");

		_statusText = statusTextGO.GetComponent<Text>();
		Assert.IsNotNull(_statusText, "couldn't find statusText component");
	}

	private void SetupLeaveRoomButton()
	{
		GameObject leaveRoomBtnGO = GameObject.Find("leave room button");
		Assert.IsNotNull(leaveRoomBtnGO, "couldn't find leave room button object");
		Button leaveRoomBtn = leaveRoomBtnGO.GetComponent<Button>();
		Assert.IsNotNull(leaveRoomBtn, "couldn't find leave room button script");

		//adding the leaveroom function ass callback, when the button is pressed.
		//could have already been added by the onClientStart (when just got into the room.
		leaveRoomBtn.onClick.AddListener(LeaveRoom);
	}
	
	/// <summary>
	/// leaves room (leads to closing the entire room)
	/// </summary>
	private void LeaveRoom()
	{
		customNetworkManager._singleton.StopHost(); //if client, disconnects, if host, disconnect and stop hosting. (this triggers the entire game to close because the host will receive a ClientDisconnected event which will close the match.)
	}

	/// <summary>
	/// Updates the game's UI such as player scores
	/// </summary>
	[Server]
	private void OnClientConnect(PendingPlayer newPlayer)
	{
		if (!_players.Contains(newPlayer))
		{
			Assert.IsTrue(_players.Count < RoomManager._singleton._numberOfExpectedPlayers, "more players than expected, ");
			_players.Add(newPlayer);

			//if the update loop hasn't started yet, start it.
			if (_leaderboardUpdateRoutine == null)
			{
				_leaderboardUpdateRoutine = StartCoroutine(UpdateLeaderBoardLoop());
			}
		}
		else
		{
			print("rejected an already connected player");
		}
	}

	/// <summary>
	/// Updates the leaderboard text to certain text.
	/// </summary>
	/// <param name="newLeaderBoardString"></param>
	[ClientRpc]
	public void RpcUpdateLeaderBoardText(string newLeaderBoardString)
	{
		GameObject playerNamesAndScoresGO = GameObject.Find("PlayerNamesAndScores");
		if (playerNamesAndScoresGO == null)
		{
			if (SceneManager.GetActiveScene().name != "_GameScene") //if it fails to find its probably because were not in the room scene, that's ok, only send if it's not ok.
				Debug.LogWarning("failed to find PlayerNamesAndScores gameobject");
		}
		else
		{
			Text leaderBoardText = playerNamesAndScoresGO.GetComponent<Text>();
			if (leaderBoardText == null)
			{
				Debug.LogWarning("failed to find PlayerNamesAndScores text component");
			}

			//update the leaderboard text to parameter.
			leaderBoardText.text = newLeaderBoardString;
		}
	}

	/// <summary>
	/// creates the leaderboard text according to
	/// the player's names and scores.
	/// </summary>
	/// <returns></returns>
	public string GetLeaderBoardText()
	{
		string leaderBoardText = "";
		foreach (PendingPlayer player in _players)
		{
			leaderBoardText += player._name + ": " + player._score + "\n";
		}
		return leaderBoardText;
	}

	[Server]
	private void SetupStartGameButton()
	{
		GameObject startGameBtnGO = GameObject.Find("start game button");
		Assert.IsNotNull(startGameBtnGO, "couldn't find start game button object");

		Button startGameBtn = startGameBtnGO.GetComponent<Button>();
		Assert.IsNotNull(startGameBtn, "couldn't find start game button script");

		//adding the startgame function ass callback, when the button is pressed.
		startGameBtn.onClick.AddListener(StartGameFromRoom);
	}

	/// <summary>
	/// assign each _player according to the rules specified in PlayerTypeOptions
	/// </summary>
	private void AssignClassesToPlayers()
	{
		foreach (PendingPlayer pendingPlayer in _players)
		{
			Assert.IsNotNull(_playerTypeOptions);

			CharacterEnum characterType = _playerTypeOptions.findNextTypeToPlay(pendingPlayer);
			pendingPlayer._characterType = characterType;
			Assert.AreNotEqual(characterType, CharacterEnum.UNSPECIFIED);
		}
	}

	/// <summary>
	/// Gets the score of each class for the last round,
	/// adds the corresponding value to each player's total score.
	/// Invoked buy the gameManager when the game finishes.
	/// </summary>
	/// <param name="bodyguardScore"></param>
	/// <param name="killerScore"></param>
	[Server]
	public void UpdatePlayersScore(int bodyguardScore, int killerScore)	  //TODO update thief and security's as well
	{
		foreach (PendingPlayer player in _players)
		{
			if (player._characterType == CharacterEnum.BODYGUARD)
			{
				player._score += bodyguardScore;
			}
			else if (player._characterType == CharacterEnum.KILLER)
			{
				player._score += killerScore;
			}
		}
	}

	
}