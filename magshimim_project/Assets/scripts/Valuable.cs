﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class Valuable
{
	private int _scoreValue;
	public int ScoreValue
	{
		get
		{
			return this._scoreValue;
		}

		protected set
		{
			this._scoreValue = value;
		}
	}

	private int _secondsToSteal;
	public int SecondsToSteal
	{
		get
		{
			return this._secondsToSteal;
		}

		protected set
		{
			this._secondsToSteal = value;
		}
	}

	private ValuablesEnum _valuableType;
	public ValuablesEnum ValuableType
	{
		get
		{
			return this._valuableType;
		}

		protected set
		{
			this._valuableType = value;
		}
	}

	public GameObject GetImagePrefab()
	{
		switch (this._valuableType)
		{
			case ValuablesEnum.DIAMOND_RING:
				return MyResources.Singleton.DiamondRingPrefab;
			case ValuablesEnum.PEARL_NECKLACE:
				return MyResources.Singleton.PearlNecklacePrefab;
			case ValuablesEnum.ART_PAINTING:
				return MyResources.Singleton.ArtPaintingPrefab;
			default:
				Debug.LogError("tried to populate box window with bad valuable input");
				return null;
		}
	}

	public static Valuable GenerateValuable(ValuablesEnum valuableEnum)
	{
		if (valuableEnum == ValuablesEnum.ART_PAINTING) return new ArtPainting();
		else if (valuableEnum == ValuablesEnum.PEARL_NECKLACE) return new PearlNecklace();
		else if (valuableEnum == ValuablesEnum.DIAMOND_RING) return new DiamondRing();
		else return null;
	}
}

