﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

/// <summary>
/// players have a point count AND a win count which is worth 150 amount of points.
/// so at the end of the match (multiple rounds, say, two)
/// the ending points of each player equals to his ingame score plus the amount of rounds he won times 150. 
/// TODO change this to include scores for each player regardless of their class. (but while modifying the value it WILL take into account the class)
/// </summary>
internal class ScoreManager : NetworkBehaviour
{
	static public ScoreManager _singleton;
	//TODO make this into syncvar?
	public int _bodyguardScore;

	public int _killerScore;

	public int _thiefScore;

	public int _securityScore;
	const int CIVILIAN_PENALTY = -25;
	const int OBJECTIVE_REWORD = 100;
	public UIManager _ui;

	private void Start()
	{
		_singleton = this;
		_bodyguardScore = 0;    // Initialize the scores at the beginning of the game
		_killerScore = 0;       // Initialize the scores at the beginning of the gamed
		_ui = UIManager._singleton;
		if (_ui == null)
		{
			Debug.LogError("_ui singleton is null");
		}
	}

	[Server]
	internal void UpdatePoints(GameObject died, string theOneWhoKilled)
	{
		//only the server should be modify the points.

		if (died == gameManager._singleton.Vip)
		{
			_killerScore += OBJECTIVE_REWORD;
		}

		if (died.tag == "killer")
		{
			_bodyguardScore += OBJECTIVE_REWORD;
		}

		if (died.tag == "bot")
		{
			if (theOneWhoKilled == "bodyguard")
				_bodyguardScore += CIVILIAN_PENALTY;
			else if (theOneWhoKilled == "security")
				_securityScore += CIVILIAN_PENALTY;
		}

		if (theOneWhoKilled == "killer" && died != gameManager._singleton.Vip)
		{
			_killerScore += CIVILIAN_PENALTY;
		}

		if (_ui == null)
		{
			Debug.LogError("_ui singleton is null");
		}
		else
		{
			UpdateScoreText();
		}
	}

	public void UpdateScoreText()
	{
		this._ui.RpcUpdateScoreText(GetScoreMessage());
	}

	public string GetScoreMessage()
	{
		return "bodyguard: " + _bodyguardScore.ToString() + " killer: " + _killerScore.ToString() + " thief: " + this._thiefScore.ToString() + " security: " + _securityScore.ToString();
	}
}