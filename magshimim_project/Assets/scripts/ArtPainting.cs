﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class ArtPainting : Valuable
{
    public ArtPainting()
    {
        this.ScoreValue = 80;
        this.SecondsToSteal = 7;
        this.ValuableType = ValuablesEnum.ART_PAINTING;
    }
}

