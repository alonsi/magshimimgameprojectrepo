﻿using Assets.scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

class ThiefController : PlayerControl
{
    #region fields
    private KeyCode _stealButton;
	#endregion

	public override CharacterEnum GetCharacterType()
	{
		return CharacterEnum.THIEF;
	}

	// Use this for initialization
	protected override void Start()
    {
        base.Start();

        _stealButton = KeyCode.Mouse0;
    }

    /// <summary>
    /// called once per frame
    /// updates _knifeTimeRemaining, if time ended, hides knife
    /// </summary>
    protected override void Update()
    {
        base.Update();

        if (isLocalPlayer)
        {
            if (Input.GetKeyDown(_stealButton)) // only triggers at the beginning of the click.
            {
                
            }
        }
    }

    /// <summary>
    /// Fires the stab on all clients.
    /// </summary>
    /// <param name="spawnPos"></param>
    /// <param name="spawnRot"></param>
    [Command]
    public void CmdSteal(GameObject boxToStealFrom)
    {
		boxToStealFrom.GetComponent<Box>().Steal();
    }

    public override string playerBeginningString()
    {
        return base.playerBeginningString() + "steal as much valuables without getting caught.";
    }
}
