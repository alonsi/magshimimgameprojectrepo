﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof (BoxCollider2D))] //for the OnTriggerEnter2D
public class bulletcontrol : NetworkBehaviour {
	public string _shooterTag;

    /// <summary>
    /// called when detects a colission.
    /// should kill if collided with a player
    /// destroys itself on impace.
    /// </summary>
    void OnTriggerEnter2D(Collider2D collidedWith)
    {
		//only the server should determine the outcome of the bullet colission
		if (!isServer)
			return;

		string collidedWithTag = collidedWith.gameObject.tag;
		if (collidedWithTag == this._shooterTag || collidedWithTag == "clue" || collidedWithTag == "knife") //the bullet should pass through his source and through clues (and through the knife).
			return;
		else
		{
			Debug.Log("bullet collided with " + collidedWith.gameObject.name);

			if (collidedWithTag == "killer" || collidedWithTag == "bot" || collidedWithTag == "security" || collidedWithTag == "thief" || collidedWithTag == "bodyguard")
			{
				gameManager._singleton.KillCharacter(collidedWith.gameObject, _shooterTag); //aka the victim
				print("about to kill character");
			}

			Destroy(this.gameObject); //destroy the bullet if it hits anything other than the above
		}	 		
	}
}