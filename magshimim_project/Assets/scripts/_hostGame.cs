﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

//used in main menu
public class _hostGame : MonoBehaviour {
    
    private uint _roomSize = 4;

    private string _roomName;

    private NetworkManager _networkManager;

    void Start()
    {		
        _networkManager = NetworkManager.singleton;
        if (_networkManager.matchMaker == null)
        {
            _networkManager.StartMatchMaker();
        }
    }

    public void setRoomName(Text name)
    {
        //Debug.Log("setting room name to: " + name.text);
        _roomName = name.text;
    }

    public void createRoom()
    {        
        if (_roomName != null && _roomName != "")
        {
            //Debug.Log("creating room: " + _roomName + " with room for " + _roomSize + " players.");
            _networkManager.matchMaker.CreateMatch(_roomName, _roomSize, true, "", "", "", 0, 0, _networkManager.OnMatchCreate);
			//TODO handle create match not working
			//load room scene
			//SceneManager.LoadScene("RoomScene");
			//the network manager does this automatically with the online-offline scene
		}
		else
        {
            Debug.Log("create room: invalid room name");
        }
    }
}
