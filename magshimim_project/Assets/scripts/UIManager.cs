﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIManager : NetworkBehaviour {

    [SerializeField]
    public Text _text;

	public Text _scoreText;

	public static UIManager _singleton;

	private void Start()
	{		
		if (_text == null)
			Debug.LogError("text component of ui not found");
		if (_scoreText == null)
			Debug.LogError("score text null");
	}

	private void Awake()
	{
		_singleton = this;
	}

	public void UpdateText(string newText)
    {
		_text.text = newText;
    }

	[ClientRpc]
	internal void RpcUpdateScoreText(string newText)
	{
		_scoreText.text = newText;
	}
}
