﻿using UnityEngine;
using UnityEngine.Assertions;

public class Helper
{
	public static string GameObjectToName(GameObject input)
	{
		return input == null ? null : input.name;
	}

	public static string ToString(Vector2 vec)
	{
		return "(" + vec.x + "," + vec.y + ")";
	}

	public static Transform CachePlayerUi()
	{
		//Initialize player UI.
		GameObject playerUiGO = GameObject.Find("PlayerUi");
		Assert.IsNotNull(playerUiGO, "couldn't find player ui");
		return playerUiGO.transform;
	}
}
