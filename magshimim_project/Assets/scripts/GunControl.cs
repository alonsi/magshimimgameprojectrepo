﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class GunControl : NetworkBehaviour {
	private float _shootingStartingDistance; //The distance the bullet is shot from (in relation to the shooter).
	public int _bulletSpeed;

	[SerializeField]
	private GameObject _bulletPrefab; //loaded in unity editor.

	private float _bulletTTL; //bullet time to live, if it doesn't collide and destroy itself, it should be destroyed after a few seconds.

	private KeyCode _shootButton;

	// Use this for initialization
	void Start () {
		Assert.IsNotNull(_bulletPrefab, this.name + ": didn't setup bullet prefab");
		_bulletSpeed = 7;
		_shootingStartingDistance = 0;
		_bulletTTL = 3;
		_shootButton = KeyCode.Mouse0;
	}
	
	// Update is called once per frame
	void Update () {
		if (isLocalPlayer)
		{
			if (Input.GetKeyDown(_shootButton)) // only triggers at the beginning of the click.
			{
				Shoot();
			}
		}
	}

	/// <summary>
	/// determines the spawn position and the direction of the bullet
	/// commands the server to spawn it.
	/// </summary>
	private void Shoot()
	{
		//Camera is needed for the mouse to worldPoint calculation.
		Camera mainCamera = FindObjectOfType<Camera>();

		//creates a ray, starting from the player, in the direction of the mouse
		Vector2 direction = mainCamera.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
		Ray2D bulletRay = new Ray2D(this.transform.position, direction);

		//The spawn position is placed on a point on the bulletRay, in a certain distance from the origin point of the ray.
		Vector2 spawnPosition = bulletRay.GetPoint(_shootingStartingDistance);
		Vector2 velocity = bulletRay.direction * _bulletSpeed;

		//Commands the server to shoot
		CmdShoot(spawnPosition, velocity);
	}

	/// <summary>
	/// Spawns the bullet on all clients, with a certain TTL.
	/// </summary>
	/// <param name="spawnPos">bullet's spawn position</param>
	/// <param name="velocity">bullet's velocity</param>
	[Command]
	public void CmdShoot(Vector2 spawnPos, Vector2 velocity)
	{
		//Spawning the bullet locally.
		GameObject newBullet = Instantiate(_bulletPrefab, spawnPos, new Quaternion());

		//Setting the velocity
		newBullet.GetComponent<Rigidbody2D>().velocity = velocity; //setting the direction of the bullet
		newBullet.GetComponent<bulletcontrol>()._shooterTag = this.tag;

		//Spawning the bullet online.
		NetworkServer.Spawn(newBullet);

		Destroy(newBullet, _bulletTTL);
	}
}
