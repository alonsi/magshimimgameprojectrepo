﻿using Assets.scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public class Clothing
    {
        public ColorEnum color;

        public Clothing()
        {
            this.color = ColorEnum.EMPTY;
        }

        public Clothing(ColorEnum color)
        {
            this.color = color;
        }
    
}
