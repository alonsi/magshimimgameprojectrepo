﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

class securityController : CluePlayerControl
{
    #region fields
    private const int KILL_REWARD = 300;
	#endregion

	public override CharacterEnum GetCharacterType()
	{
		return CharacterEnum.SECURITY;
	}

	protected override Clue GetClue()
    {
        return CluesManager._singleton.GetClueForSecurity();
    }

    public override string playerBeginningString()
    {
        return base.playerBeginningString() + "find and kill the thief before he escapes.";
    }
}

