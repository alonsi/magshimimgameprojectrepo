﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class PlayerTypeOptions
{
    private Queue<CharacterEnum> playerOptions; // All player type options
    private List<CharacterEnum> firstGameOptions;

    public PlayerTypeOptions() // Creates the playerOptions queue with all of the player type options in accordance to the playerEnum constants we decided on
    {
        playerOptions = new Queue<CharacterEnum>();
        firstGameOptions = new List<CharacterEnum>();
        CharacterEnum[] playerEnums = (CharacterEnum[])Enum.GetValues(typeof(CharacterEnum));

        foreach (CharacterEnum playerEnum in playerEnums)
        {
			if (playerEnum == CharacterEnum.SECURITY && RoomManager.WITH_SECURITY
				|| playerEnum == CharacterEnum.THIEF && RoomManager.WITH_THIEF
				|| playerEnum == CharacterEnum.KILLER && RoomManager.WITH_KILLER
				|| playerEnum == CharacterEnum.BODYGUARD && RoomManager.WITH_BODYGUARD)
			{
                playerOptions.Enqueue(playerEnum);
                firstGameOptions.Add(playerEnum);
            }
        }
    }

    public CharacterEnum findNextTypeToPlay(PendingPlayer pendingPlayer) 
    {
        CharacterEnum pendingPlayerType = pendingPlayer._characterType;
        CharacterEnum nextType = CharacterEnum.UNSPECIFIED;

        if (pendingPlayerType == CharacterEnum.UNSPECIFIED) // First round: generate randomly
        {
			nextType = RandomGenerator<CharacterEnum>.generateItem(firstGameOptions); 
			//generate item also removes it from the list, so no 2 player will get the same class.
		}
        else // Every round except for the first: find the next type by going through the queue of player types
        {
            for(int i=0 ; i<playerOptions.Count() ; i++)
            {
                CharacterEnum nextInQueue = playerOptions.Dequeue();
                playerOptions.Enqueue(nextInQueue);

				//forgot the logic behind this part.
                if (nextInQueue == pendingPlayerType)
                {
                    nextType = playerOptions.Peek();
                }
            }
        }
        return nextType;
    }
}
