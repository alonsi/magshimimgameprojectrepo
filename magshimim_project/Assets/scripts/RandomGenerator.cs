﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

static class RandomGenerator<T>
{
	public static T generateItem(List<T> list)
	{
		int generatedIndex = new System.Random().Next(list.Count); // TODO: Change to Unity Random?!?
		T value = list[generatedIndex];
		list.RemoveAt(generatedIndex);
		return value;
	}

	public static ValuablesEnum GenerateRandomValuableEnum()
	{ 		
		//fully random, does work
		List<ValuablesEnum> valuables = new List<ValuablesEnum>{ ValuablesEnum.ART_PAINTING, ValuablesEnum.DIAMOND_RING, ValuablesEnum.PEARL_NECKLACE };
		int index = UnityEngine.Random.Range(0, valuables.Count());
		return valuables[index];
	}
}