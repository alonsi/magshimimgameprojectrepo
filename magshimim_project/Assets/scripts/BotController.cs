﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

//This bots are handled by the server alone

[RequireComponent(typeof(Seeker))]
public class BotController : CharacterControl
{
	#region fields
	private GameObject _target; //The bot's current target (it follows it)

	[SerializeField]
	private float _waypointMaxDistance = 0.5f; //Once the distance between the bot and the current waypoint is smaller than this value, start heading towords the next waypoint.

	float _distanceToChangeTarget = 5f; //Once the distance between the bot and it's target is smaller than this value, choose a new target.

	public float _updateRate = 2; //number of updates per second

	private Seeker _seeker; //the A* component

	public Path _path; //The calculated path

	private int _currWayPointIndex;
	#endregion

	/// <summary>
	/// Initializes the fields
	/// starts the path-updating loop
	/// </summary>
	protected override void Start()
	{
		base.Start();
		if (!isServer)
			return;
		_target = null;
		_seeker = this.GetComponent<Seeker>();
		_currWayPointIndex = 0;


		//coroutine to avoid the function from cloging up the game.
		StartCoroutine(UpdatePath());
	}

	/// <summary>
	/// a coroutine to update the path to the target
	/// in a certain amount of times per second
	/// </summary>
	private IEnumerator UpdatePath()
	{
		//calculates the distance between the player and the target
		float distance = 0;
		if (_target != null)
		{
			distance = Vector2.Distance(this.transform.position, _target.transform.position);
		}

		//If theres no target, or if it reached the target, should roll the next one.
		if (_target == null || distance < _distanceToChangeTarget)
		{
			RollNextTarget();
		}

		if (_seeker == null)
		{
			Debug.LogError("_seeker is null");
		}
		else if (_target == null)
		{
			Debug.LogError("_target is null");
		}
		else
		{
			_seeker.StartPath(this.transform.position, _target.transform.position, OnPathComplete);
		}

		//pauses for some times then, loops.
		yield return new WaitForSeconds(1f / _updateRate);
		StartCoroutine(UpdatePath());
	}

	/// <summary>
	/// avoid error caused by trying to run coroutine
	/// when the object is dead
	/// </summary>
	private void OnDisable()
	{
		StopAllCoroutines();
	}

	/// <summary>
	/// callback for seeker's "startPath" method
	/// initializes the fields according to the new path.
	/// </summary>
	/// <param name="p">the new path</param>
	private void OnPathComplete(Path p)
	{
		if (p.error)
		{
			Debug.LogError("path error: " + p.errorLog);
			_path = null;
		}
		else
		{
			_path = p;
			_currWayPointIndex = 0;
		}
	}

	private void RollNextTarget()
	{
		//requests all the potential targets
		List<GameObject> potentialTargets = gameManager._singleton.GetReleventGameObjects();

		//don't get the same target again.
		if (_target != null)
			potentialTargets.Remove(_target);

		//don't get myself as a target.
		potentialTargets.Remove(this.gameObject); 

		//if the vector ain't empty
		if (potentialTargets.Count > 0)
		{
			//set the target as a random object from the vector.
			int randomIndex = UnityEngine.Random.Range(0, potentialTargets.Count);
			_target = potentialTargets[randomIndex];
		}
		else
		{
			Debug.Log("bot couldn't find a target");
		}
	}

	/// <summary>
	/// called every frame
	/// moves the object towords his target
	/// </summary>
	void Update()
	{
		//only the server should control the bots, and only if the game had already started.
		if (isServer && gameManager._singleton.GameStarted) 
		{
			if (_target == null || _path == null || IsPathFinished())
			{
				_velocity = Vector2.zero;
				return; //a new target and path will be rolled on the next UpdatePath.
			}
			else
			{
				//get the next waypoint from the path
				Vector3 currWayPoint = _path.vectorPath[_currWayPointIndex];
				Vector3 dir = (currWayPoint - this.transform.position).normalized;
				_velocity = dir;

				if (Vector2.Distance(this.transform.position, _path.vectorPath[_currWayPointIndex]) < _waypointMaxDistance)
				{
					_currWayPointIndex++;
				}
			}
		}		
	}

	private bool IsPathFinished()
	{
		return _currWayPointIndex >= _path.vectorPath.Count;
	}
}