﻿#define ASSERT
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System;
using System.Collections;
using UnityEngine.Assertions;

public delegate void ClientConnectedEventHandler(PendingPlayer newPlayer);
public delegate void ClientDisconnectEventHandler(); //when a client disconnects (network manager calls this)
public delegate void NetworkSceneLoadedEventHandler(string sceneName);

public class customNetworkManager : NetworkManager
{
	#region fields
	public event ClientConnectedEventHandler ClientConnected;
	public event ClientDisconnectEventHandler ClientDisconnected;
	public event NetworkSceneLoadedEventHandler NetworkSceneLoaded;
	public static customNetworkManager _singleton;
	#endregion

	public override void OnServerSceneChanged(string sceneName)
	{
		base.OnServerSceneChanged(sceneName);
		if (NetworkSceneLoaded != null)
		{
			print("sending networkscene loaded event");
			NetworkSceneLoaded(sceneName); //Triggers the event.
		 }
	}

	public void Start()
	{
		_singleton = this;

#if ASSERT
		//makes sure the offline and online scenes are set properly.
		Assert.AreEqual(this.offlineScene, "_LobbyScene", "network manager's offline scene must be set to the lobby scene");
		Assert.AreEqual(this.onlineScene, "RoomScene", "network manager's online scene must be set to the room scene");
		//makes sure the spawnable prefabs are set properly (order doesn't matter).
		List<String> requiredSpawnablePrfabs = new List<string>{ "bodyGuardPrefab", "bulletPrefab", "botPrefab", "cluePrefab", "killerPrefab", "thiefPrefab", "securityPrefab" };
		foreach (string prefabName in requiredSpawnablePrfabs)
		{
			bool setProperly = spawnPrefabs.ConvertAll(Helper.GameObjectToName).Contains(prefabName);
			string message = "didn't set " + prefabName + " as a spawnable prefab.";
			Assert.IsTrue(setProperly, message);
		}
#endif
	}

	/// <summary>
	/// This is invoked by ClientScene.AddPlayer.
	/// Adds the user to the game as a pending player. (doesn't yet have a character)
	/// </summary>
	override public void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		StartCoroutine(TriggerClientConnectedRoutine(new PendingPlayer(conn, playerControllerId)));
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
	{
		string extraMessage = extraMessageReader.ReadString();
		PendingPlayer newPendingPlayer = new PendingPlayer(conn, playerControllerId, extraMessage, 0);
		StartCoroutine(TriggerClientConnectedRoutine(newPendingPlayer));
	}

	/// <summary>
	/// loops until someone (the gameManager) subscribes to the client connect event,
	/// then, trigger that event.
	/// this is important because the host will connect, and trigger the event before the host's
	/// gameManager will subscribe to the event.
	/// </summary>
	/// <param name="newPlayer"></param>
	/// <returns></returns>
	private IEnumerator TriggerClientConnectedRoutine(PendingPlayer newPlayer)
	{
		//loops until someone subscribes
		while (ClientConnected == null) //null aka no subscribers
		{
			yield return null; //waits a frame to avoid cloging up the game.
		}

		//triggers the event.
		ClientConnected(newPlayer);
	}

	/// <summary>
	/// invoked on server when a client disconnects.
	/// </summary>
	/// <param name="conn"></param>
	public override void OnServerDisconnect(NetworkConnection conn)
	{
		base.OnServerDisconnect(conn);
		//print("triggering on client disconnect event");
		ClientDisconnected(); //triggers the event
	}

	internal List<GameObject> SpawnPendingPlayers(Dictionary<PendingPlayer, GameObject> playersObjects)
	{
		List<GameObject> instances = new List<GameObject>();
		foreach (KeyValuePair<PendingPlayer, GameObject> player_object in playersObjects)
		{
			GameObject characterInstance = SpawnPlayer(player_object);
			instances.Add(characterInstance);
		}

		return instances;
	}

	/// <summary>
	/// spawns the pending players
	/// </summary>
	/// <param name="player_object"></param>
	/// <returns></returns>
	private GameObject SpawnPlayer(KeyValuePair<PendingPlayer, GameObject> player_object)
	{
		//spawns locally.
		GameObject playerInstance = Instantiate(player_object.Value);

		//sets the user as the controller of the gameObject.
		NetworkServer.AddPlayerForConnection(player_object.Key._connection, playerInstance, player_object.Key._playerControllerId);

		//spawns online.
		NetworkServer.Spawn(playerInstance);
		return playerInstance;
	}

	/// <summary>
	/// Invoked whenever a client connects (on the client game instance)
	/// Sets the connection as ready and adds to it.
	/// </summary>
	/// <param name="conn"></param>
	public override void OnClientConnect(NetworkConnection conn)
	{
		ClientScene.Ready(conn);

		//gets the player's chosen name from the UI object.
		GameObject playerPreferencesGO = GameObject.Find("PlayerPreferences");
		if (playerPreferencesGO != null)
		{
			PlayerPreferences playerPreferences = playerPreferencesGO.GetComponent<PlayerPreferences>();
			if (playerPreferences != null)
			{
				ClientScene.AddPlayer(conn, 0, new StringMessage(playerPreferences.PlayerName));
				return;
			}
		}

		//only if the method hasn't returned yet
		//adds as player (invokes OnServerAddPlayer)
		//the playerControllerId is 0, because every game instance has just one player.

		ClientScene.AddPlayer(conn, 0);
	}

	/// <summary>
	/// overridden because it made the clients declare themselves as ready and it would mess the flow of the code.
	/// </summary>
	/// <param name="conn"></param>
	public override void OnClientSceneChanged(NetworkConnection conn)
	{
		if (ClientScene.ready == false)
		{
			ClientScene.Ready(conn);
		}
	}
}