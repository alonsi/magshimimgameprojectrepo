﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Assets.scripts;
using UnityEngine.Assertions;
using System.Linq;

//TODO this class should probably only exist on the server, perhaps seperate this into a client-gameManager and a server-GameManager.
public class gameManager : NetworkBehaviour
{
	#region fields
	[SerializeField]
	List<Transform> _spawningPositions;

	[SerializeField]
	GameObject _killerPrefab;

	[SerializeField]
	GameObject _bodyGuardPrefab;

	[SerializeField]
	GameObject _thiefPrefab;

	[SerializeField]
	GameObject _securityPrefab;

	[SerializeField]
	private GameObject _botPrefab;

	public static gameManager _singleton;

    public AudioSource _audioSource;
    public AudioClip DeathSound;

    private bool _gameStarted;
	public bool GameStarted { get { return _gameStarted; } }

	//instances of spawned gameObjects
	[SerializeField]
	private List<GameObject> _boxInstances; //added in unity editor cus it's static
	private List<GameObject> _botInstances;
	private List<GameObject> _playerInstances;
	private GameObject _vip; // killer's target
	public GameObject Vip
	{
		get
		{
			return _vip;
		}
	}
	#endregion

	private void OnEnable()
	{
		//asserts all spawnpoints were set
		Assert.IsNotNull(_spawningPositions, "didn't set any spawn positions");
		Assert.IsTrue(_spawningPositions.Count >= 2 * RoomManager._singleton._numberOfExpectedPlayers, "didn't set enough spawn position, expected: " + (2 * RoomManager._singleton._numberOfExpectedPlayers).ToString());

		Assert.IsNotNull(_killerPrefab, "gameManager: didn't set killer prefab");
		Assert.IsNotNull(_bodyGuardPrefab, "gameManager: didn't set bodyguard prefab");
		Assert.IsNotNull(_thiefPrefab, "gameManager: didn't set thief prefab");
		Assert.IsNotNull(_securityPrefab, "gameManager: didn't set security prefab");

		Assert.IsNotNull(_botPrefab, "gameManger: didn't set bot prefab properly");

		//TODO add assert on the boxes.
	}

	//TODO maybe we can move the Start to awake, and the stargame to be on Start (Without coroutine)
	private void Start()
	{
		if (isServer)
		{
			//WaitForPlayersToBeReady();
			//print("all ready so starting");

			StartGame(RoomManager._singleton._players);
		}
	}

	/// <summary>
	/// loops until all players in room are ready
	/// this doesn't work properly (sometimes it just freezes the game)
	/// </summary>
	private static void WaitForPlayersToBeReady()
	{
		bool allReady;
		do
		{
			allReady = true;
			foreach (PendingPlayer currPlayer in RoomManager._singleton._players)
			{
				if (!currPlayer._connection.isReady)
				{
					print("set as not all ready");
					allReady = false;
				}
			}
		} while (allReady);
	}

	private void Awake()
	{
		_singleton = this;
        _audioSource = GetComponent<AudioSource>();

		_gameStarted = false;
		_botInstances = new List<GameObject>();
		_playerInstances = new List<GameObject>();
		//boxes is initialized in unity editor cus boxes are static
	}

	/// <summary>
	/// Gets the instances the game-relevent gameobjects
	/// </summary>
	internal List<GameObject> GetReleventGameObjects()
	{
		List<GameObject> interactables = new List<GameObject>();
		interactables.AddRange(_botInstances);
		interactables.AddRange(_playerInstances);
		interactables.AddRange(_boxInstances);


		Assert.IsNotNull(CluesManager.instatiatedClue, "instantiated clue is null");
		if (CluesManager.instatiatedClue != null) //This could happen when a bot finds a target before ClueSetUp finished.
			interactables.Add(CluesManager.instatiatedClue);
		return interactables;
	}

	/// <summary>
	/// only invoked on the server cus only "OnServerAddPlayer" invokes it.
	/// starts the game:
	/// spawns the bots and players, updates _gameStarted
	/// NOTE: the gameobjects of the players in the beginning are the prefabs to spawn. the gameobjects after the spawn are the actual instances
	/// </summary>
	[Server]
	public void StartGame(List<PendingPlayer> pendingPlayers)
	{
		print("starting game");

		//setup the clothing combinations for the new players to choose from.
		clothingManager.possibleClothingCombinations = clothingManager.calculatePossibleClothingCombinations();

		//player gameobjects to setup before spawning
		List<GameObject> objectsToSetSpawnPoint = new List<GameObject>();

		//give each player the gameObject of his class.
		Dictionary<PendingPlayer, GameObject> playersObjects = new Dictionary<PendingPlayer, GameObject>();
		foreach (PendingPlayer player in pendingPlayers)
		{
			playersObjects.Add(player, GetPlayerObjectByClass(player._characterType));
		}

		objectsToSetSpawnPoint.AddRange(playersObjects.Values);

		SpawnBotsLocally();

		//add the newly spawned bots
		objectsToSetSpawnPoint.AddRange(_botInstances);

		//assign the spawnpoints
		AssignSpawnPoints(objectsToSetSpawnPoint);

		//spawn the players (locally and online)
		List<GameObject> playerInstances = customNetworkManager._singleton.SpawnPendingPlayers(playersObjects);

		_playerInstances.AddRange(playerInstances);

		//Recolors the players so that each one matches one bot in clothing
		StartCoroutine(RecolorBots());

		//spawn the bots online
		SpawnBotsOnline(_botInstances);

		//choose a VIP
		ChooseVip();

		StartCoroutine(StartUpClues());

		_gameStarted = true;
	}

	private IEnumerator StartUpClues()
	{
		//maybe needs to add another yield return null, because maybe it can still run before RecolorBots coroutine.2
		yield return null;
		//generate the clues
		CluesManager._singleton.GenerateClues();

		//NOTE: possibly StartCoroutine is needed here
		CluesManager._singleton.ClueStart();
	}

	private GameObject GetPlayerObjectByClass(CharacterEnum gameClass)
	{
		switch (gameClass)
		{
			case CharacterEnum.KILLER:
				return _killerPrefab;
			case CharacterEnum.BODYGUARD:
				return _bodyGuardPrefab;
			case CharacterEnum.THIEF:
				return _thiefPrefab;
			case CharacterEnum.SECURITY:
				return _securityPrefab;
			default:
				Debug.LogError("GetPlayerObjectByClass should only return player classes");
				return null;
		}
	}

	[Server]
	public IEnumerator RecolorBots()
	{
		//wait for Update to finish, for the character's clothingManger could finish setting their colors
		//see script lifecycle flowchart: https://docs.unity3d.com/Manual/ExecutionOrder.html
		yield return null;

		Assert.AreEqual(_botInstances.Count, RoomManager._singleton._numberOfExpectedPlayers, "number of bot instances != number of expected players");
		Assert.AreEqual(_playerInstances.Count, RoomManager._singleton._numberOfExpectedPlayers, "number of player instances != number of expected players");

		for (int i = 0; i < RoomManager._singleton._numberOfExpectedPlayers; i++)
		{
			clothingManager playerClothingManager = _playerInstances[i].GetComponent<clothingManager>();
			ColorEnum playerPantsColor = playerClothingManager.getPantsColor();
			ColorEnum playerShirtColor = playerClothingManager.getShirtColor();

			Assert.AreNotEqual(playerPantsColor, ColorEnum.EMPTY, "tried to set bot color with empty color");
			Assert.AreNotEqual(playerShirtColor, ColorEnum.EMPTY, "tried to set bot color with empty color");

			clothingManager botClothingManager = _botInstances[i].GetComponent<clothingManager>();
			botClothingManager.setColors(playerPantsColor, playerShirtColor);
		}
	}


	/// <summary>
	/// currently only bots, perhaps have other players as targets aswell
	/// </summary>
	private void ChooseVip()
	{
		//generate random index
		int randomIndex = UnityEngine.Random.Range(0, new List<GameObject>().Count);

		//update the vip
		_vip = _botInstances[randomIndex];
		_vip.GetComponentInChildren<TextMesh>().text = "VIP";

		print("vip is: " + _vip.gameObject.name);
	}

	/// <summary>
	/// choose a name (botX) and spawn each bot (locally)
	/// </summary>
	private void SpawnBotsLocally()
	{
		int bot0TagIndex = 2; //as seen on the tags specified in the A* gameobject.

		for (int i = 0; i < RoomManager._singleton._numberOfExpectedPlayers; i++)
		{
			//spawn the bot object locally
			GameObject spawnedBot = Instantiate(_botPrefab);

			//give them different names for easier debugging.
			spawnedBot.name = "bot" + i.ToString();

			//could be on bot script
			Seeker seeker = spawnedBot.GetComponent<Seeker>();
			if (seeker == null)
			{
				Debug.LogError("couldn't find seeker");
			}
			else
			{
				//this next algorithm is quite tricky... prepare yourself.

				int basicGroundMask = 1;

				//the tags are represented in a bitMask. if bot0's index is 2, than enabling his tag means setting the third bit as true.

				//pushing 1 bot0's tag plus one times left.
				//if bot0TagIndex = 2, and i = 0, than shifting 2 times left...
				//1, aka 0001 becomes 0100.
				int currentBotTag = 1 << (bot0TagIndex + i);

				//or operatore combines the masks together. both basicGround AND the current bot's tag are enabled.
				seeker.traversableTags = basicGroundMask | currentBotTag;
			}

			//TODO maybe try graphupdateobject like they recommend here https://arongranberg.com/astar/docs/graph-updates.php

			Pathfinding.GraphUpdateScene currGus = spawnedBot.GetComponentInChildren<Pathfinding.GraphUpdateScene>();

			//should modify the nodes around him
			currGus.modifyTag = true;

			//here we choose what is the tag that the bot will modify the nodes into.
			//It looks as if setTag is represented by a regular int of the index, instead of the weird bitMask thingy above.
			//simply setting it to the current bot's index.
			currGus.setTag = bot0TagIndex + i;

			//update the list
			_botInstances.Add(spawnedBot);
		}
	}

	private void SpawnBotsOnline(List<GameObject> botPrefabs)
	{
		foreach (GameObject botPrefab in botPrefabs)
		{
			NetworkServer.Spawn(botPrefab);
		}
	}

	/// <summary>
	/// assign spawn points to each character, according to the "spawnPosition" objects in the scene.
	/// round robin algorithm, aka spawn one on each point.
	/// </summary>
	/// <param name="characters"></param>
	private void AssignSpawnPoints(List<GameObject> characters)
	{
        Assert.IsTrue(characters.Count <= _spawningPositions.Count, "Not enough spawn positions");

		foreach (GameObject currCharacter in characters)
		{
			if (currCharacter == null)
			{
				Debug.LogError("AssignSpawnPoints tries to modify null references");
			}
			else
			{
				currCharacter.transform.position = RandomGenerator<Transform>.generateItem(_spawningPositions).position;
			}
		}
	}



	/// <summary>
	/// TODO: remove from the respective list
	/// TODO: give/remove points
	/// TODO: maybe make the killer disappear as well (if won).
	/// </summary>
	/// <param name="toKill"></param>
	public void KillCharacter(GameObject toKill, string theOneWhoKilled)
	{
		ScoreManager._singleton.UpdatePoints(toKill, theOneWhoKilled);

        //play death sound
        _audioSource.PlayOneShot(DeathSound, 1);

        //remove the instances from the lists
        _botInstances.Remove(toKill);
		_playerInstances.Remove(toKill);


		if (toKill == _vip)
		{
			print("end the game, winner is killer");
			StartCoroutine(EndGame());

		}
		else if (toKill.tag == "killer")
		{
			print("end the game, winner is bodyguard");
			StartCoroutine(EndGame());
		}
		else if (toKill.tag == "thief")
		{
			print("end the game, winner is security");
			StartCoroutine(EndGame());
		}

		if (toKill.tag != "bot")
            toKill.GetComponent<PlayerControl>().Die();
        else
		    Destroy(toKill);
	}

	/// <summary>
	/// ends the game by switching to roomscene
	/// uptdates the RoomManager about the points each class got.
	/// </summary>
	[Server]
	private IEnumerator EndGame()
	{
        yield return new WaitForSeconds(4);
		RoomManager._singleton.UpdatePlayersScore(ScoreManager._singleton._bodyguardScore, ScoreManager._singleton._killerScore);
		customNetworkManager._singleton.ServerChangeScene("RoomScene");
		print("swapped from game scene to room scene");
	}

	/// <summary>
	/// called when this is destroyed
	/// destroys the roomManager(clone) which is sometimes created
	/// dynamically for faster debugging
	/// </summary>
	private void OnDestroy()
	{
		_singleton = null;
		GameObject spawnedRoomManager = GameObject.Find("RoomManager(Clone)");
		if (spawnedRoomManager != null) //if exists
		{
			Destroy(spawnedRoomManager); //cus another one will
		}
	}

}