﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

//will be on the box object.
public class Box : NetworkBehaviour
{
	#region fields
	[SyncVar (hook = "OnValuableModified")]
	private ValuablesEnum _valuableEnum;
	internal ValuablesEnum ValuableEnum
	{
		get
		{
			return _valuableEnum;
		}
	}

    private Valuable _valuableInBox; // Valuable object
	public Valuable ValuableInBox
	{
		get
		{
			return this._valuableInBox;
		}
	}		 

	private List<GameObject> _observers;

	public const float BOX_TO_PLAYER_MAX_RANGE = 3f; //once the player gets too far from the box it closes, this variable defines the distance which is considered too far.
	
	[SerializeField]
	public GameObject BOX_WINDOW_PREFAB;

	public Transform _playerUi;

	

	[SerializeField] AudioClip _boxOpeningClip;
	[SerializeField] AudioClip _boxClosingClip;
	#endregion

	#region Input
	private void Update()
	{
		//iterate over every observer, if the distance between him and the box, remove him as an observer.
		
	}

	private void OnMouseOver()
	{
		PlayerControl.LocalPlayerGO.GetComponent<PlayerBoxController>()._selectedBox = this;
	}

	private void OnMouseExit()
	{
		PlayerControl.LocalPlayerGO.GetComponent<PlayerBoxController>()._selectedBox = null;
	}

	#endregion
	

	private void Awake()
	{
		_playerUi = Helper.CachePlayerUi();
	}	

	private void Start()
	{
		//can't do these stuff on Awake because "isServer" returns false.
		if (isServer)
		{
			//TODO set valuable as random valuable (maybe make some rarer than others)
			_observers = new List<GameObject>();
            _valuableEnum = RandomGenerator<ValuablesEnum>.GenerateRandomValuableEnum();
            _valuableInBox = Valuable.GenerateValuable(_valuableEnum);
		}
	}

	[Server]
	public void AddObserver(GameObject newObservingCharacter)
	{
		if (_observers.Count == 0)
		{
			//the box was closed and now is opened so:
			RpcOpenBox();
		}
		//TODO make sure it doesn't already exist in list.
		_observers.Add(newObservingCharacter);
	}

	/// <summary>
	/// play the box's opening animation and sound
	/// </summary>
	[ClientRpc]
	private void RpcOpenBox()
	{
		print("<playing box OPENING animation and sound>");
		gameManager._singleton._audioSource.PlayOneShot(this._boxOpeningClip, 0.5f);
	}

	/// <summary>
	/// called by a player that wants to close the box,
	/// or by the server when it detects a player got out of range.
	/// removes it from the observers list and
	/// plays closing animation if needed
	/// </summary>
	/// <param name="observerToRemove"></param>
	[Server]
	public void RemoveObserver(GameObject observerToRemove)
	{
		_observers.Remove(observerToRemove);
		if (_observers.Count == 0)
		{
			//no one is observing the box aka holding it open
			RpcCloseBox();
		}
	}

	/// <summary>
	/// play the box's closing animation and sound
	/// </summary>
	[ClientRpc]
	private void RpcCloseBox()
	{
		print("<playing box CLOSING animation and sound>");
		gameManager._singleton._audioSource.PlayOneShot(this._boxClosingClip, 0.5f);
	}
	
	/// <summary>
	/// Change the valuable enum to empty on all clients
	/// add points according to the valuable
	/// the 'waiting x seconds' part is done in BoxWindowControl.StartStealing
	/// </summary>
	public void Steal()
	{
		ScoreManager._singleton._thiefScore += _valuableInBox.ScoreValue; //swap 1 with the valable's point parameter.
		_valuableEnum = ValuablesEnum.EMPTY;  //ValuableInBox is changed by _valuableEnum's hook
		ScoreManager._singleton.UpdateScoreText();
	}

	private void OnValuableModified(ValuablesEnum newValuable)
	{
		this._valuableEnum = newValuable;
		this._valuableInBox = ValuablesEnumHelper.GetValuable(newValuable);
	}
}
