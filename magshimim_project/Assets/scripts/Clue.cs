﻿using Assets.scripts;

/// <summary>
/// represents the content of the clue
/// </summary>
public class Clue
{
	public ColorEnum _color;
	public ClothingEnum _cloth;
	public CharacterEnum _characterType;

	public Clue(ColorEnum color, ClothingEnum cloth, CharacterEnum characterType)
	{
		_color = color;
		_cloth = cloth;
		_characterType = characterType;
	}
}
