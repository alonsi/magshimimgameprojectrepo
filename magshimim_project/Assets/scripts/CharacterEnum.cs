﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;

public enum CharacterEnum
{

	UNSPECIFIED, //null basically
	KILLER,
	BODYGUARD,
	VIP, //TODO remove this! this isn't the point of character enum. replace with BOT
	THIEF,
	SECURITY
}

public static class ValuablesEnumHelper
{
	/// <summary>
	/// Get character enum of input character object.
	/// </summary>
	internal static CharacterEnum GetEnum(GameObject characterObject)
	{
		CharacterControl controller = characterObject.GetComponent<CharacterControl>();
		Assert.IsNotNull(controller, "didn't find controller script on character object");
		return controller.GetCharacterType();
	}

	internal static Valuable GetValuable(ValuablesEnum newValuable)
	{
		switch (newValuable)
		{
			case ValuablesEnum.DIAMOND_RING:
				return new DiamondRing();
			case ValuablesEnum.PEARL_NECKLACE:
				return new PearlNecklace();
			case ValuablesEnum.ART_PAINTING:
				return new ArtPainting();
			default:
				return null;
		}
	}
}