﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterControl : NetworkBehaviour
{
	[SerializeField]
	protected float _speed;

	protected UIManager _ui;

	Rigidbody2D _rb;

	protected Vector2 _velocity;

	public virtual CharacterEnum GetCharacterType()
	{										 
		return CharacterEnum.UNSPECIFIED;
	}

	//Use this for initialization
	protected virtual void Start()
	{
		_velocity = Vector2.zero;
		_rb = GetComponent<Rigidbody2D>();
		_speed = 1.5f;
		_ui = UIManager._singleton;
	}

	private void FixedUpdate()
	{
		//if this is the the local player, or if its the server instance and it's a bot (Cus server controls all bots)
		if (isLocalPlayer || isServer && tag == "bot")
		{
			_rb.MovePosition(_rb.position + _velocity.normalized * Time.fixedDeltaTime * _speed);
		}
	}
}
