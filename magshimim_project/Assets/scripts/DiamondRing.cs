﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class DiamondRing : Valuable
{
    public DiamondRing()
    {
        this.ScoreValue = 50;
        this.SecondsToSteal = 5;
        this.ValuableType = ValuablesEnum.DIAMOND_RING;
    }
}

