﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum ValuablesEnum
{
	UNSPECIFIED, //aka uninitialized (because this is the first one, it is the default value)
	EMPTY,
	DIAMOND_RING,
	PEARL_NECKLACE,
	ART_PAINTING
}
