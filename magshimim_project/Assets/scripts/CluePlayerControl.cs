﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//TODO make it into an interface
public abstract class CluePlayerControl : PlayerControl
{

	/// <summary>
	/// Gets the apropriate clue for the class
	/// </summary>
	/// <returns>clue</returns>
	protected abstract Clue GetClue();

	/// <summary>
	/// Parses the clue according to the class
	/// </summary>
	/// <param name="c">clue to parse</param>
	/// <returns>parsed clue</returns>
	public string ParseClue(Clue c)
	{
		if (c != null)
		{
			return "the " + c._characterType + " has " + c._color.ToString() + " " + c._cloth.ToString();
		}
		else
		{
			return "no clue found";
		}
	}

	/// <summary>
	/// Updates UI of the player which picked up a clue
	/// </summary>
	/// <param name="clue"></param>
	[ClientRpc]
	public void RpcClue(string clue)
	{
		//prevent updating on all clients.
		if (isLocalPlayer)
		{
			_ui.UpdateText(clue);
		}
	}

	//TODO a problem might accour if a class inheriting from CluePlayerControl implements OnTriggerEnter2D itself. (this code will not invoke).
	//     so perhaps use base.Ontriggerenter2d when implementing it in super classes.
	/// <summary>
	/// Handles picking up a clue,
	/// Server checks for colission and updates on the collided player
	/// </summary>
	/// <param name="collidedWith"></param>
	[Server]
	void OnTriggerEnter2D(Collider2D collidedWith)
	{
		//makes sure collided with an actual clue
		if (collidedWith.gameObject.tag == "clue")
		{
			//gets the clue
			Clue c = GetClue();

			//parses it
			string parsedClue = ParseClue(c);

			//updates it on the correct client.
			RpcClue(parsedClue);
		}
	}
}