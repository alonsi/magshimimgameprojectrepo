﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes knife work: kills only bots and players.
/// Also, acts as a shield against bullets, since is tagged as obstacle.
/// </summary>
public class Knife : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collidedWith)
    {
		//TODO: ignore colission with knife
        string tag = collidedWith.gameObject.tag;
        if (tag != "killer")
        {
            print("knife collided with " + collidedWith.gameObject.name);
            if (tag == "bodyguard" || tag == "bot") //can only kill bots and bodyguard														   
                gameManager._singleton.KillCharacter(collidedWith.gameObject, "killer"); //aka the victim
        }
    }
}

