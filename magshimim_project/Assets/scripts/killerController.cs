﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class killerController : CluePlayerControl {
	#region fields
	[SerializeField]
	private const float KNIFE_TTL = 0.5f;
	[SerializeField]
	private KeyCode _stabButton;
	private float _knifeTimeRemaining;
	private float _stabStartingDistance = 1.5f;
	GameObject _knife;
    private AudioSource source;
    public AudioClip clip;

	#endregion

	public override CharacterEnum GetCharacterType()
	{
		return CharacterEnum.KILLER;
	}

	// Use this for initialization
	protected override void Start()
	{
		base.Start();

        source = GetComponent<AudioSource>();
        _stabButton = KeyCode.Mouse0;
		_knifeTimeRemaining = 0;
		_knife = FindKnife();
        _knife.SetActive(false);
	}

	/// <summary>
	/// called once per frame
	/// updates _knifeTimeRemaining, if time ended, hides knife
	/// </summary>
	protected override void Update()
	{
		base.Update();
		if (_knifeTimeRemaining > 0)
		{
            if(_knife.activeInHierarchy)
			    //updates time remaining.
			    _knifeTimeRemaining = _knifeTimeRemaining - Time.deltaTime;

			//if ended
			if (_knifeTimeRemaining <= 0)
			{
				//disable (hide) it
				_knife.SetActive(false);
			}
		}

		if (isLocalPlayer)
		{
			if (Input.GetKeyDown(_stabButton)) // only triggers at the beginning of the click.
			{
                Stab();
			}
		}
	}

	/// <summary>
	/// Enables the knife and moves it towards the mouse, pointing to that direction aswell.
	/// it gets disabled again in the Update function.
	/// </summary>
	private void Stab()
	{
		//Camera is needed for the mouse to worldPoint calculation.
		Camera mainCamera = FindObjectOfType<Camera>();

		//creates a ray, starting from the player, in the direction of the mouse
		Ray2D bulletRay = new Ray2D(this.transform.position, mainCamera.ScreenToWorldPoint(Input.mousePosition) - this.transform.position);

		//The spawn position is placed on a point on the bulletRay, in a certain distance from the origin point of the ray.
		Vector2 spawnPosition = bulletRay.GetPoint(_stabStartingDistance);

		//calculate rotation of the knife using trigonometry.
		var angle = Mathf.Atan2(bulletRay.direction.y, bulletRay.direction.x) * Mathf.Rad2Deg - 90;
		Quaternion spawnRotation = Quaternion.AngleAxis(angle, Vector3.forward);

		//Commands the server to stab
		CmdStab(spawnPosition, spawnRotation);
	}

	/// <summary>
	/// stab animation (invoked on all clients when called on server)
	/// </summary>
	/// <param name="spawnPos"></param>
	/// <param name="spawnRot"></param>
	[ClientRpc]
	void RpcStab(Vector2 spawnPos, Quaternion spawnRot)
	{
        source.PlayOneShot(clip, 1);
		_knifeTimeRemaining = KNIFE_TTL;
		GameObject knifeObject = _knife;
		knifeObject.SetActive(true);
		knifeObject.transform.position = spawnPos;
		knifeObject.transform.rotation = spawnRot;
	}

	/// <summary>
	/// Fires the stab on all clients.
	/// </summary>
	/// <param name="spawnPos"></param>
	/// <param name="spawnRot"></param>
	[Command]
	void CmdStab(Vector2 spawnPos, Quaternion spawnRot)
	{
		RpcStab(spawnPos, spawnRot);
	}

	private GameObject FindKnife()
	{
		return this.transform.Find("Knife").gameObject;
	}

	/// <summary>
	/// requests a clue from the ClueManager, describing the VIP
	/// </summary>
	/// <returns></returns>
	protected override Clue GetClue()
	{
		return CluesManager._singleton.GetClueForKiller();
	}

    public override string playerBeginningString()
    {
        return base.playerBeginningString() + "kill the VIP.";
    }
}
