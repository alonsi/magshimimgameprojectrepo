﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class bodyGuardController : CluePlayerControl
{
    #region fields

    public int _bulletSpeed;

    [SerializeField]
    private GameObject _bulletPrefab; //loaded in unity editor.

    private float _bulletTTL; //bullet time to live, if it doesn't collide and destroy itself, it should be destroyed after a few seconds.

    private KeyCode _shootButton;
	#endregion

	public override CharacterEnum GetCharacterType()
	{
		return CharacterEnum.BODYGUARD;
	}

	//TODO maybe instead of overriding, just make GetClue an implemented method that calls the correct function according to the _classEnum field.
	protected override Clue GetClue()
    {
        return CluesManager._singleton.GetClueForBodyguard();
    }

    public override String playerBeginningString()
    {
        return base.playerBeginningString() + "protect the VIP.";
    }
}
