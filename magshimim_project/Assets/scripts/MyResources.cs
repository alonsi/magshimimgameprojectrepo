﻿using UnityEngine;

//This class will contain references to any resources that didn't find a place elsewhere
public class MyResources : MonoBehaviour
{
	private static MyResources _singleton;
	public static MyResources Singleton
	{
		get
		{
			return _singleton;
		}
	}

	private void Awake()
	{
		if (_singleton == null)
			_singleton = this;
		else
			Debug.LogError("tried to create multiple of singleton: MyResources");
	}

	#region TreasureCache
	[SerializeField]
	private GameObject _diamondRingPrefab;
	public GameObject DiamondRingPrefab
	{
		get
		{
			return _diamondRingPrefab;
		}
	}

	[SerializeField]
	private GameObject _artPaintingPrefab;
	public GameObject ArtPaintingPrefab
	{
		get
		{
			return _artPaintingPrefab;
		}
	}

	[SerializeField]
	private GameObject _pearlNecklacePrefab;
	public GameObject PearlNecklacePrefab
	{
		get
		{
			return _pearlNecklacePrefab;
		}
	}
	#endregion	

	[SerializeField]
	private GameObject _stealButtonPrefab;
	public GameObject StealButtonPrefab
	{
		get
		{
			return _stealButtonPrefab;
		}
	}


	[SerializeField]
	private GameObject _stealTimerPrefab;
	public GameObject StealTimerPrefab
	{
		get
		{
			return this._stealTimerPrefab;
		}
	}
}
