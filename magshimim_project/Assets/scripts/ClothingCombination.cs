﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public class ClothingCombination
    {
        public Clothing pants;
        public Clothing shirt;

        public ClothingCombination()
        {
            this.pants = null;
            this.shirt = null;
        }

        public ClothingCombination(Clothing pants, Clothing shirt)
        {
            this.pants = pants;
            this.shirt = shirt;
        }

    
}