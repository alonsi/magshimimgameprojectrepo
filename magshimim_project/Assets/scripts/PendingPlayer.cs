﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

/// <summary>
/// This class represents the user
/// the name isn't very good, but im afraid to change it (fucking git and unity)
/// </summary>
public class PendingPlayer
{
	public readonly NetworkConnection _connection;
	public readonly short _playerControllerId;
	public string _name;
	public int _score;
	public CharacterEnum _characterType;
	private static int _genericNameSuffix = 1;
	private const string GENERIC_NAME_PREFIX = "player";

	#region c'tors

	public static void ResetNameSuffix()
	{
		_genericNameSuffix = 1;
	}

	public PendingPlayer(NetworkConnection connection, short playerControllerId)
	{
		_connection = connection;
		_playerControllerId = playerControllerId;
		_characterType = CharacterEnum.UNSPECIFIED;
		_name = GetNextGenericName();
		_score = 0;
	}

	public override bool Equals(object obj)
	{

		if (obj == null || GetType() != obj.GetType())
		{
			return false;
		}

		// TODO: write your implementation of Equals() here
		PendingPlayer other = (PendingPlayer) obj;
		return _connection.connectionId == other._connection.connectionId;
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	/// <summary>
	/// generates the next generic name according to the name prefix and suffix fields: <prefix><incrementing suffix>
	/// </summary>
	/// <returns></returns>
	private string GetNextGenericName()
	{
		return GENERIC_NAME_PREFIX + _genericNameSuffix++.ToString();
	}

	public PendingPlayer(NetworkConnection connection, short playerControllerId, string name, int score)
	{
		_connection = connection;
		_playerControllerId = playerControllerId;
		if (name == "")
		{
			_name = GetNextGenericName();
		}
		else
		{
			_name = name;
		}
		_score = score;
		_characterType = CharacterEnum.UNSPECIFIED;
	}

	internal static List<NetworkInstanceId> ConvertToNetworkIdList(List<GameObject> gameObjects)
	{
		List<NetworkInstanceId> netIds = new List<NetworkInstanceId>();
		foreach (GameObject instance in gameObjects)
		{
			netIds.Add(instance.GetComponent<NetworkIdentity>().netId);
		}
		return netIds;
	}

	#endregion
}