﻿#define ASSERT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Assets.scripts;
using UnityEngine.Assertions;

/// <summary>
/// Manages the clues for the entire game
/// only server has an instacnce of this
/// </summary>
public class CluesManager : NetworkBehaviour
{
	public static CluesManager _singleton;

	List<Clue> _killerClues; // Clues about the VIP and the bodyguard
	List<Clue> _bodyguardClues; // Clues about the VIP and the killer
	List<Clue> _securityClues; // Clues about the thief

	[SerializeField]
    private GameObject cluePrefab;
    static public GameObject instatiatedClue;

	[SerializeField] Transform _clueSpawnPointsObj; //Reference to the "folder object" containing the objects that represent a clue spawn point.
	Queue<Transform> _clueSpawnPointsQueue; //Queue of spawn point's positions.

	// Use this for initialization
	void Awake()
	{
		Assert.IsNotNull(cluePrefab, "didn't set the cluePrefab");
		_singleton = this;
		_killerClues = new List<Clue>();
		_bodyguardClues = new List<Clue>();
		_securityClues = new List<Clue>();

		_clueSpawnPointsQueue = new Queue<Transform>();

		for (int i = 0; i < _clueSpawnPointsObj.childCount; i++)
		{
			_clueSpawnPointsQueue.Enqueue(_clueSpawnPointsObj.GetChild(i)); //Populates the queue with the "folder object"'s children.
		}
	}

	/// <summary>
	/// Generates all the clues.
	/// used as a coroutine because it needs to be able to wait for the Update to finish.
	/// </summary>
	/// <returns></returns>
	public void GenerateClues()
	{
		if (RoomManager.WITH_KILLER && RoomManager.WITH_BODYGUARD)
		{
			// Killer clues
			_killerClues.AddRange(GenerateBodyGuardClues()); // Add clues about bodyguard
			_killerClues.AddRange(GenerateVIPClues()); // Add clues about VIP
			
			// Bodyguard clues
			_bodyguardClues.AddRange(GenerateKillerClues()); // Add clues about killer
			_bodyguardClues.AddRange(GenerateVIPClues()); // Add clues about VIP

		}

		if (RoomManager.WITH_THIEF && RoomManager.WITH_SECURITY)
		{
			// Security clues
			_securityClues.AddRange(GenerateThiefClues());
		}
	}

	/// <summary>
	/// generates clues about the character
	/// </summary>
	/// <param name="character"> the character the clues will describe </param>
	/// <returns> the clues describing the character</returns>
	private static List<Clue> GenerateCluesAboutCharacter(GameObject character, CharacterEnum characterType)
	{
		//creating the clues according to the killer's colors
		clothingManager clothes = character.GetComponent<clothingManager>();
		Assert.IsNotNull(clothes, "couldn't find clothing manager on: " + character.name);
		Clue clue1 = new Clue(clothes.getPantsColor(), ClothingEnum.PANTS, characterType);
		Clue clue2 = new Clue(clothes.getShirtColor(), ClothingEnum.SHIRT, characterType);

		if (clue1._color == ColorEnum.EMPTY || clue2._color == ColorEnum.EMPTY)
		{
			Debug.LogError("tried to generate clues before colors were initialized");
		}

		List<Clue> retList = new List<Clue> { clue1, clue2 };
		return retList;
	}

	/// <summary>
	/// Creating the clues for the VIP
	/// </summary>
	/// <param name="VIP"></param>
	private List<Clue> GenerateVIPClues()
	{
		GameObject vip = gameManager._singleton.Vip;

		Assert.IsNotNull(vip, "tried to generate vip clues before it was set");
		return GenerateCluesAboutCharacter(vip, CharacterEnum.VIP);
	}

	/// <summary>
	/// Creating the clues for the killer
	/// </summary>
	/// <param name="killer"></param>
	private List<Clue> GenerateKillerClues()
	{
		GameObject killer = GameObject.FindWithTag("killer");

		if (killer == null)
		{
			Debug.LogError("couldn't find the killer");
		}
		else
		{
			return GenerateCluesAboutCharacter(killer, CharacterEnum.KILLER);
		}
		return null;
	}

	/// <summary>
	/// Creating the clues for the bodyguard
	/// </summary>
	/// <param name="bodyguard"></param>
	private List<Clue> GenerateBodyGuardClues()
	{
		GameObject bodyguard = GameObject.FindWithTag("bodyguard");

		Assert.IsNotNull(bodyguard, "couldn't find bodyguard gameobjects");

		return GenerateCluesAboutCharacter(bodyguard, CharacterEnum.BODYGUARD);
	}

	/// <summary>
	/// Creating the clues for the thief
	/// </summary>
	/// <param name="thief"></param>
	private List<Clue> GenerateThiefClues()
	{
		GameObject thief = GameObject.FindWithTag("thief");

		if (thief == null)
		{
			Debug.LogError("couldn't find the thief");
		}
		else
		{
			return GenerateCluesAboutCharacter(thief, CharacterEnum.THIEF);
		}
		return null;
	}

	/// <summary>
	/// returns a clue about the vip or the bodyguard.
	/// the same clue won't be given twice
	/// (probably called for the killer)
	/// </summary>
	/// <returns>
	/// A clue describing the vip
	/// </returns>
	public Clue GetClueForKiller()
	{
		if (_killerClues.Count == 0)
			return null;

		Clue toReturn = RandomGenerator<Clue>.generateItem(_killerClues);
		CmdUpdatePosition();
		return toReturn;
	}

	public Clue GetClueForSecurity()
	{
		if (_securityClues.Count == 0)
		{
			return null;
		}

		Clue toReturn = RandomGenerator<Clue>.generateItem(_securityClues);
		CmdUpdatePosition();
		return toReturn;
	}

	/// <summary>
	/// returns a clue about the vip or the killer.
	/// the same clue won't be given twice
	/// (probably called for the bodyguard)
	/// </summary>
	/// <returns>
	/// A clue describing the killer
	/// </returns>
	public Clue GetClueForBodyguard()
	{
		if (_bodyguardClues.Count == 0)
			return null;

		Clue toReturn = RandomGenerator<Clue>.generateItem(_bodyguardClues);
		CmdUpdatePosition();
		return toReturn;
	}

	public void ClueStart()
	{
		Vector2 newPosition = GetNextCluePosition();
		instatiatedClue = Instantiate(cluePrefab);
		instatiatedClue.transform.position = newPosition;
		NetworkServer.Spawn(instatiatedClue);
	}

	private Vector2 GetNextCluePosition()
	{
		Transform nextClueTransform = _clueSpawnPointsQueue.Peek(); //gets the next transform
		_clueSpawnPointsQueue.Enqueue(_clueSpawnPointsQueue.Dequeue()); //pushes it back to the beginning, it's a circular queue.
		return nextClueTransform.position;
	}

	[Command]
    public void CmdUpdatePosition()
    {
        Vector2 newPosition = GetNextCluePosition();
        instatiatedClue.GetComponent<Rigidbody2D>().MovePosition(newPosition);
    }
}
