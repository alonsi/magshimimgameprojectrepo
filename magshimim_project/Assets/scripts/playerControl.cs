﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;
using UnityEngine.UI;

public abstract class PlayerControl : CharacterControl
{
	#region fields

	[SerializeField]
	private KeyCode _upButton;
	[SerializeField]
	private KeyCode _downButton;
	[SerializeField]
	private KeyCode _leftButton;
	[SerializeField]
	private KeyCode _rightButton;
	
	public GameObject _cameraPrefab;
    protected GameObject _cam;
	private int score;
    public bool inGameDestroy = false;

	
    Animator _animator;
    bool _myIsLocalPlayer; //because 'isLocalPlayer' doesn't work in Die() for some reason;

	private static GameObject _localPlayerGO = null;

	public static GameObject LocalPlayerGO
	{
		get
		{
			return _localPlayerGO;
		}
	}
	#endregion

	// Use this for initialization
	protected override void Start()
	{
		if (isLocalPlayer)
		{
			print("setting up _localPlayerGO");
			_localPlayerGO = this.gameObject;
			SetupStatusText(playerBeginningString());
		}

		Assert.IsNotNull(_cameraPrefab, "didn't set up camera prefab on: " + this.name);

        base.Start();

        //set camera as a child so it follows player
        if (isLocalPlayer)
        {
            _cam = Instantiate(_cameraPrefab, this.transform);
            _cam.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -7);
        }

        _animator = GameObject.Find("PlayerUi/Busted").GetComponent<Animator>();
        _myIsLocalPlayer = isLocalPlayer;

        _upButton = KeyCode.W;
		_downButton = KeyCode.S;
		_leftButton = KeyCode.A;
		_rightButton = KeyCode.D;
		score = 0;
	}

	private void SetupStatusText(string newText)
	{
		GameObject.Find("StatusText").GetComponent<Text>().text = newText;
	}

	/// <summary>
	/// moves the player according to input,
	/// </summary>
	protected virtual void Update()
	{
		//prevent this code running on player objects of other players.
		if (isLocalPlayer)
		{
			_velocity = Vector2.zero;

			//updates the movind direction according to input.
			if (Input.GetKey(_upButton))
			{
				_velocity += Vector2.up;
			}

			if (Input.GetKey(_downButton))
			{
				_velocity += Vector2.down;
			}

			if (Input.GetKey(_leftButton))
			{
				_velocity += Vector2.left;
			}

			if (Input.GetKey(_rightButton))
			{
				_velocity += Vector2.right;
			}

			//character control does the actual moving.
		}
	}

	public int getScore()
	{
		return this.score;
	}

	/// <summary>
	/// This function is called when the player is destroyed, which occurs when some other player killed
	/// the player with a weapon
	///     -In this case the player should see a BUSTED text and become a spectator of the conituing game.
	/// To become a spectator, the player must instanciate a camera. NOTE: This could theoretically
	/// be done by detaching the existing camera from player, but it doesn't work here.
	/// </summary>
	public void Die()
	{
		print("die: " + this.gameObject.name);
		if (_myIsLocalPlayer)
		{
			//create another camera so player can keep viewing the scene
			_cam = Instantiate(_cameraPrefab);
			_cam.GetComponent<Camera>().orthographicSize = 15;
			_cam.transform.position.Set(0, 2, -1);

			//Set BUSTED sign
			_animator.SetTrigger("isGameOver");
		}

		Destroy(this.gameObject);
	}

    public virtual String playerBeginningString()
    {
        return "You are the " + GetCharacterType() + ". Your mission is to ";
    }
}
