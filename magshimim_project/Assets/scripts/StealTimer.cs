﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StealTimer : MonoBehaviour {
	private Box _boxToStealFrom;
	internal float _originalTimeToWait;
	private float _timePassed;
	private bool _stole;

	[SerializeField] Image _loadingBar;

	// Use this for initialization
	void Start () {
		_stole = false;
		this._timePassed = 0;
		_loadingBar.fillAmount = 1; //aka full
	}

	private void Update()
	{
		if (this._timePassed >= this._originalTimeToWait // finished waiting
			&& !_stole)								     // and didn't steal yet
		{
			ThiefController thief = PlayerControl.LocalPlayerGO.GetComponent<ThiefController>();
			thief.CmdSteal(this._boxToStealFrom.gameObject);
			_stole = true;
		}
		else
		{
			this._timePassed += Time.deltaTime;
			this._loadingBar.fillAmount = this._timePassed / this._originalTimeToWait;
		}
	}

	internal static StealTimer Create(float secondsToWait, Box boxToStealFrom, Transform parent)
	{
		//instantiate the prefab
		GameObject stealTimerGO = Instantiate(MyResources.Singleton.StealTimerPrefab, parent);

		//extract the script
		StealTimer stealTimer = stealTimerGO.GetComponent<StealTimer>();

		//initialize the script
		stealTimer._originalTimeToWait = secondsToWait;
		stealTimer._boxToStealFrom = boxToStealFrom;

		return stealTimer;
	}
}
