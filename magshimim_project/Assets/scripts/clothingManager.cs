﻿using Assets.scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



//the fields only contain the index of the color using the ColorEnum
//then, the actual color is set according to those fields.
public class clothingManager : NetworkBehaviour {

	public static List<ClothingCombination> possibleClothingCombinations;

	[SyncVar (hook = "OnPantsUpdate"), SerializeField]
	private ColorEnum _pantsColor;

	[SyncVar (hook = "OnShirtUpdate"), SerializeField]
	private ColorEnum _shirtColor;

	Dictionary<ClothingEnum, SpriteRenderer> _clothesSR;

	private void Awake()
	{
		InitializeSpriteRenderers();
	}

	// Use this for initialization
	void Start () {
		//The server should choose the colors in order for it to be the same on all game instances.
		if (isServer //The server is responsible for choosing the colors
			&& this.tag != "bot") //the colors are only chosen for players (later, the gameManager sets the bot's colors to correspond to the player's colors.)
		{
			ChooseColors();
		}
	}

	private void InitializeSpriteRenderers()
	{
		_clothesSR = new Dictionary<ClothingEnum, SpriteRenderer>();
		//get the spriteRenderers of all the character's children.
		SpriteRenderer[] clothes = GetComponentsInChildren<SpriteRenderer>();

		//add the clothes to _clothesSR
		foreach (SpriteRenderer currCloth in clothes)
		{
			if (currCloth.name == "pants")
			{
				_clothesSR.Add(ClothingEnum.PANTS, currCloth);
			}
			else if (currCloth.name == "shirt")
			{
				_clothesSR.Add(ClothingEnum.SHIRT, currCloth);
			}
			//else its not a clothing object, could be the knife for example, either way - don't do anything.
		}
	}

	/// <summary>
	/// Sets the clothing to random colors.
	/// Only runs on server, to have identical colors on all game clients.
	/// </summary>
	[Server]
    void ChooseColors()
    {
        ClothingCombination clothingCombination = chooseColorCombination();

		setColors(clothingCombination.pants.color, clothingCombination.shirt.color);
	}

    [Server]
    internal void setColors(ColorEnum pantsColor, ColorEnum shirtColor)
    {
        this._pantsColor = pantsColor;
        this._shirtColor = shirtColor;
    }

    public ColorEnum getPantsColor()
    {
        return _pantsColor;
    }

    public ColorEnum getShirtColor()
    {
		return _shirtColor;
    }



	/// <summary>
	/// Chooses a color combination from the possible combination list and then removes the combination from the list
	/// </summary>
	/// <returns>the generated color</returns>
	ClothingCombination chooseColorCombination()
	{
		return RandomGenerator<ClothingCombination>.generateItem(possibleClothingCombinations);
	}

	/// <summary>
	/// Updates a certain clothing to a chosen color
	/// </summary>
	/// <param name="name">the clothing to update</param>
	/// <param name="newColor">the new color</param>
	void UpdateCertainClothing(ClothingEnum clothToUpdate, ColorEnum newColor)
    {
		this._clothesSR[clothToUpdate].color = GetColorByEnum(newColor);
    }

    static Color GetColorByEnum(ColorEnum colorEnum)
    {
        if (colorEnum == ColorEnum.GREEN)
        {
            return Color.green;
        }
        else if(colorEnum == ColorEnum.RED)
        {
            return Color.red;
        }
		else
		{
			return Color.gray;
		}
    }

    public static List<ClothingCombination> calculatePossibleClothingCombinations()
    {
        List<ClothingCombination> possibleClothes = new List<ClothingCombination>(); // Creates List of all the possible clothing combinations to return in end of function
        
        foreach (Clothing pants in getVariations()) // Run on all the pants variations
        {
            foreach (Clothing shirt in getVariations()) // Run on all the shirt variations
            {
                possibleClothes.Add(new ClothingCombination(pants, shirt)); // Add every possible pant-shirt combination to the list
            }
        }

        return possibleClothes;
    }

    public override String ToString()
    {
        return "I have " + this._pantsColor + " pants and a " + this._shirtColor + "shirt";
    }

    public static List<Clothing> getVariations()
    {
        var colors = Enum.GetValues(typeof(ClothingEnum)); // Creates an enumerator of all the colors in the ColorEnum type
        List<Clothing> clothingVariations = new List<Clothing>(); // Creates List of clothing variations to return in end of function

        foreach(ColorEnum color in colors)
        {
            if(color != ColorEnum.EMPTY) clothingVariations.Add(new Clothing(color)); // Creates a new Cloth for each color and add to the list
        }

        return clothingVariations;
    }

	private void OnPantsUpdate(ColorEnum newColor)
	{
		//print("in pants hook, new color is " + newColor.ToString() + "though the variabl's value is " + _pantsColor.ToString());
		_pantsColor = newColor;
		UpdateCertainClothing(ClothingEnum.PANTS, newColor);
	}

	private void OnShirtUpdate(ColorEnum newColor)
	{
		//print("in shirt hook, new color is " + newColor.ToString() + "though " + _shirtColor.ToString());
		_shirtColor = newColor;
		UpdateCertainClothing(ClothingEnum.SHIRT, newColor);
	}
}
