﻿#define ASSERT
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.Networking.Match;
using UnityEngine.Assertions;

//used in main menu
public class joinGameScript : MonoBehaviour {
    private NetworkManager _networkManager;

    //need this for deleting former items while refreshing.
    private List<GameObject> _roomList;

    [SerializeField]
    private GameObject _roomListItem;

    [SerializeField]
    private Transform _roomListParent;

    // Use this for initialization
    void Start () {
		Assert.IsNotNull(_roomListItem, this.name + ": didn't set room list item");
		Assert.AreEqual<string>("Room List Item", _roomListItem.name);
		Assert.IsNotNull(_roomListParent, this.name + ": didn't set room list parent");

		_roomList = new List<GameObject>();
        _networkManager = NetworkManager.singleton;
        if (_networkManager.matchMaker == null)
        {
            _networkManager.StartMatchMaker();
        }

        refreshRoomList();
    }
	
	public void refreshRoomList()
    {
        clearRoomList();
        _networkManager.matchMaker.ListMatches(0, 20, "", false, 0, 0, OnMatchList);
        //Debug.Log("refreshing");
    }

    private void clearRoomList()
    {
        //clears the current room list
        for (int i = 0; i < _roomList.Count; i++)
        {
            Destroy(_roomList[i]); //destroying the objects
        }
        _roomList.Clear(); //clearing the references
    }

    private void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        if (success)
        {
            //Debug.Log("number of matches found:" + matchList.Count);
            
            foreach (MatchInfoSnapshot match in matchList)
            {
                GameObject currentRoomListItem = Instantiate<GameObject>(_roomListItem);
                roomListItemScript currentRoomListItemScript = currentRoomListItem.GetComponent<roomListItemScript>();

                //adds the item to the scroll view list.
                currentRoomListItem.transform.SetParent(_roomListParent);

                //setup the script (so it will be aware of the match parameters)
                currentRoomListItemScript.setup(match);

                _roomList.Add(currentRoomListItem);
            }
        }
        else
        {
            Debug.Log("getting matchlist failed");
        }
    }
}
