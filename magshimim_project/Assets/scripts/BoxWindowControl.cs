﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

//This will be attached to the box window object.
public class BoxWindowControl : MonoBehaviour {

	Box _box; //the box this window represents
	CharacterEnum _observer;

	//returns a BoxWindowControl instance, linked to the parameter box.
	public static BoxWindowControl Create(Box box, CharacterEnum observerCharacterType)
	{
		//instantiate the prefab
		GameObject boxWindowGo = Instantiate(box.BOX_WINDOW_PREFAB, box._playerUi);

		//extract the script
		BoxWindowControl boxWindow = boxWindowGo.GetComponent<BoxWindowControl>();

		//initialize the script
		boxWindow._box = box;
		boxWindow._observer = observerCharacterType;
		return boxWindow;
	}

	// Use this for initialization
	void Start ()
	{
		Assert.IsNotNull(this._box, "BoxWindow's box is null");
		Assert.AreNotEqual(this._observer, CharacterEnum.UNSPECIFIED, "BoxWindow's observer is null");
		if (this._box.ValuableInBox != null)
		{
			//Populates the window with the apropriate image to the valuable in the box.
			GameObject ValuablePrefab = this._box.ValuableInBox.GetImagePrefab();
			Instantiate(ValuablePrefab, this.transform);

			if (this._box.ValuableEnum != ValuablesEnum.EMPTY)
			{
				if (this._observer == CharacterEnum.THIEF)
				{
					CreateStealButton();
				}
				else
				{
					print("didn't add steal button cus you're not the thief");
				}
			}
			else
			{
				print("didn't add steal button cus the box is empty");
			}
		}
	}

	private void CreateStealButton()
	{
		Assert.IsNotNull(MyResources.Singleton.StealButtonPrefab, "steal button prefab is null");

		GameObject stealButtonGO = Instantiate(MyResources.Singleton.StealButtonPrefab, this.transform);
		Button stealButton = stealButtonGO.GetComponent<Button>();
		Assert.IsNotNull(stealButton, "steal button game object didn't have \"Button\" script");

		stealButton.onClick.AddListener(this.OnClickStealButton);
		print("should add box steal function to button now");
	}

	/// <summary>
	/// Wrapper function of StartStealing,
	/// because a button's OnClick can't be a coroutine
	/// </summary>
	private void OnClickStealButton()
	{
		float secondsToWait = this._box.ValuableInBox.SecondsToSteal;
		StealTimer.Create(secondsToWait, _box, this.transform);
	}
}
