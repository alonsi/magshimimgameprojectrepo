﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

//used in main menu
public class roomListItemScript : NetworkBehaviour {

    private MatchInfoSnapshot _match;

    [SerializeField]
    private Text _roomDescriptionText;

    //setups the text and button click according to the match.
	public void setup(MatchInfoSnapshot match)
    {
        _match = match;
        _roomDescriptionText.text = match.name + " (" + match.currentSize + "/" + match.maxSize + ")";
    }

    //TODO po siamnu et ha hafifa
    public void joinRoom()
    {
        NetworkManager.singleton.matchMaker.JoinMatch(_match.networkId, "", "", "", 0, 0, NetworkManager.singleton.OnMatchJoined);
		//Load Room Scene
		//SceneManager.LoadScene("RoomScene");
		//the network manager does this automatically with the online-offline scene
    }
}
