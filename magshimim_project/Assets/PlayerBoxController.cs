﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class PlayerBoxController : NetworkBehaviour {
	internal Box _selectedBox;
	private Box _observedBox; //TODO theres probably a way to use just _selectedBox
	private const KeyCode OPEN_BOX_KEY_CODE = KeyCode.Mouse1;
	private const KeyCode CLOSE_BOX_KEY_CODE = KeyCode.Space;
	private BoxWindowControl _instantiatedBoxWindow;

	// Use this for initialization
	void Start () {
		_selectedBox = null;
	}
	
	// Update is called once per frame
	void Update () {
		if (isLocalPlayer)
		{
			if (Input.GetKeyDown(OPEN_BOX_KEY_CODE)
				 && _instantiatedBoxWindow == null) //and if player isn't already viewing a box
			{
				if (CanCharacterReachBox(_selectedBox.transform, this.transform))
				{
					print("detected open box: " + this.name);
					StartObserving();
				}
			}

			if (Input.GetKeyDown(CLOSE_BOX_KEY_CODE))
			{
				if (_instantiatedBoxWindow != null)
				{
					StopObserving();
				}
			}
		} 		
	}

	//TODO there will probably be a bug where the same player observes twice.
	public void StartObserving()
	{
		Assert.IsNotNull(_selectedBox.BOX_WINDOW_PREFAB, "box window prefab isn't initialized");
		Assert.IsNotNull(_selectedBox._playerUi, "player ui isn't initialized");

		//get the new observer's character type
		CharacterEnum characterType = ValuablesEnumHelper.GetEnum(this.gameObject);
		Assert.AreNotEqual(characterType, CharacterEnum.UNSPECIFIED, "character's characterEnum is invalid");
		Assert.AreNotEqual(characterType, CharacterEnum.VIP, "character's characterEnum is invalid");

		_instantiatedBoxWindow = BoxWindowControl.Create(_selectedBox, characterType);
		_observedBox = _selectedBox;
		print("commanding to add observer");
		CmdAddObserver(this.gameObject, _observedBox.gameObject);
	}

	[Command]
	private void CmdAddObserver(GameObject newObservingCharacter, GameObject box)
	{
		print("recieved add commannd");
		box.GetComponent<Box>().AddObserver(newObservingCharacter);
	}

	public void StopObserving()
	{
		Assert.IsNotNull(_instantiatedBoxWindow, "no box window to destroy");
		Destroy(_instantiatedBoxWindow.gameObject);
		_instantiatedBoxWindow = null;
		CmdStopObserving(this.gameObject, _observedBox.gameObject);
		_observedBox = null;
	}

	[Command]
	private void CmdStopObserving(GameObject quittingObserver, GameObject box)
	{
		box.GetComponent<Box>().RemoveObserver(quittingObserver);
	}

	//fix TODO this always returns true cus 'this' is no the player.
	//according to BOX_TO_PLAYER_MAX_RANGE
	static private bool CanCharacterReachBox(Transform character, Transform box)
	{
		return Vector2.Distance(character.position, box.position) < Box.BOX_TO_PLAYER_MAX_RANGE;
	}
}
