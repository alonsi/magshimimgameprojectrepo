﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfinderManager : MonoBehaviour {
	private float _updateRate = 3f;

	// Use this for initialization
	void Start () {
		StartCoroutine(MyScan());
	}

	private void OnDestroy()
	{
		this.StopAllCoroutines();
	}

	/// <summary>
	/// updates the pathfinding
	/// takes into account the effect of moving bots (checkout the child "graphUpdateScene" on botPrefab and botController)
	/// </summary>
	/// <returns></returns>
	private IEnumerator MyScan()
	{
		yield return new WaitForSeconds(1f / _updateRate);

		AstarPath.active.Scan();
		//pauses for some times then, loops.
		StartCoroutine(MyScan());
	}
}
