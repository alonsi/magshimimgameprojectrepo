﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class chatboxScript : MonoBehaviour {
    public GameObject _Content;
    public Text _linePrefab;
    public static chatboxScript _singleton;
    public static GameObject _triggeringButton;
    public GameObject _NoneStatictriggeringButton;

    void Start()
    {
        if (_singleton != null)
        {
            Debug.Log("tried to create an additional chatboxScript");
        }
        else
        {
            _singleton = this;
        }
        _triggeringButton = _NoneStatictriggeringButton;
    }

    public void addLine(string line)
    {
        Debug.Log(line);
        Text tempLine = _linePrefab;
        tempLine.text = line;
        Instantiate(_linePrefab, _Content.transform);
        Debug.Log("added line: " + line);
    }
}