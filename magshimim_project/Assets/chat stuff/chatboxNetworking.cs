﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class chatboxNetworking : NetworkBehaviour
{
    public InputField _messageInputField;

    void Start()
    {
        if (!isLocalPlayer)
            return;

        _messageInputField = GameObject.Find("Message InputField").GetComponent<InputField>();
        if (_messageInputField == null)
        {
            Debug.Log("the input field is null yo");
        }

        if (chatboxScript._triggeringButton == null)
        {
            Debug.Log("the button is null");
        }

        chatboxScript._triggeringButton.GetComponent<Button>().onClick.AddListener(callCmdSendMessage);
    }

    public void callCmdSendMessage()
    {
        if (!isLocalPlayer)
            return;
        CmdSendMessage(_messageInputField.text);
    }

    [Command]
    public void CmdSendMessage(string message)
    {
        Debug.Log("command that calls the rpc");
        RpcSendMessage(message);
    }

    [ClientRpc]
    public void RpcSendMessage(string message)
    {
        if (chatboxScript._singleton == null)
        {
            Debug.Log("chatboxscript.singleton is null");
        }
        else
        {
            chatboxScript._singleton.addLine(message);
        }          
    }

    void OnConnectedToServer()
    {
        Debug.Log("Connected to server2");
    }
}
