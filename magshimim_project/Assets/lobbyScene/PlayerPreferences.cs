﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPreferences : MonoBehaviour {

	private string _playerName;
	private static PlayerPreferences _singleton;

	public string PlayerName
	{
		get
		{
			return _playerName;
		}
	}

	public static PlayerPreferences Singleton
	{
		get
		{
			return _singleton;
		}
	}

	private void Awake()
	{
		if (_singleton != null)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_singleton = this;

			DontDestroyOnLoad(this);
			//if the name isn't edited, it will stay "". Pending player give them a generic name.
		}
	}

	private void OnDestroy()
	{
		//if the original was destroyed
		if (_singleton == this)
		{
			//reset singleton
			_singleton = null;
		}
	}

	public void UpdateName(string newName)
	{
		_playerName = newName;
	}
}
